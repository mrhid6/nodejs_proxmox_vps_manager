var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('index.hbs', {
        layout: "main.hbs"
    });
});

/* GET pve nodes. */
router.get('/pvenodes', function (req, res, next) {
    res.render('pvenodes.hbs', {
        layout: "main.hbs"
    });
});

/* GET pve storage. */
router.get('/pvestorage', function (req, res, next) {
    res.render('pvestorage.hbs', {
        layout: "main.hbs"
    });
});

/* GET resourceplans. */
router.get('/resourceplans', function (req, res, next) {
    res.render('resourceplans.hbs', {
        layout: "main.hbs"
    });
});

/* GET ipaddresses. */
router.get('/ipaddresses', function (req, res, next) {
    res.render('ipaddresses.hbs', {
        layout: "main.hbs"
    });
});


/* GET newvm. */
router.get('/newvm', function (req, res, next) {
    res.render('newvm.hbs', {
        layout: "main.hbs"
    });
});

/* GET listvms. */
router.get('/listvms', function (req, res, next) {
    res.render('listvms.hbs', {
        layout: "main.hbs"
    });
});

/* GET vmtemplates. */
router.get('/vmtemplates', function (req, res, next) {
    res.render('vmtemplates.hbs', {
        layout: "main.hbs"
    });
});


/* GET clients. */
router.get('/clients', function (req, res, next) {
    res.render('clients.hbs', {
        layout: "main.hbs"
    });
});

module.exports = router;