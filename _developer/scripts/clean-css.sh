#!/bin/bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)"
BASEDIR="${DIR}/../.."

CSS_DIR="${BASEDIR}/public/stylesheets"

echo -en "\nDashboard CSS ... "
cleancss -o ${CSS_DIR}/main.min.css ${CSS_DIR}/main.css ${CSS_DIR}/layout.css ${CSS_DIR}/wizard.css
echo -en "Finished!\n"
