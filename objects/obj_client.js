class Client {
    constructor(id, email, password, firstname, lastname) {
        this._id = id;
        this._email = email;
        this._password = password;
        this._firstname = firstname;
        this._lastname = lastname;
    }

    getId() {
        return this._id;
    }

    getEmail() {
        return this._email;
    }

    getPassword() {
        return this._password;
    }

    getFirstName() {
        return this._firstname;
    }

    getLastName() {
        return this._lastname;
    }

    getFullName() {
        return this.getFirstName() + " " + this.getLastName()
    }
}

module.exports = Client;