class PVEStorage {
    constructor(id, name, shared, allowed_content) {
        this._id = id;
        this._name = name;
        this._shared = shared;
        this._allowed_content = allowed_content
    }

    getId() {
        return this._id;
    }

    getName() {
        return this._name;
    }

    isShared() {
        return this._shared;
    }

    getAllowedContent() {
        return this._allowed_content;
    }

    isOSStorage() {
        return (this.getAllowedContent().indexOf("rootdir") > -1)
    }

    isTemplateStorage() {
        return (this.getAllowedContent().indexOf("vztmpl") > -1)
    }
}

module.exports = PVEStorage;