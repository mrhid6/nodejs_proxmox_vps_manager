class PVENode {
    constructor(id, name, status) {
        this._id = id;
        this._name = name;
        this._status = status;
        this._storage = [];
    }

    getId() {
        return this._id;
    }

    getName() {
        return this._name;
    }

    getStatus() {
        return this._status;
    }

    getStorage() {
        return this._storage
    }

    addStorage(storage) {
        this._storage.push(storage);
    }

    hasStorageWithId(id) {
        return this.getStorage().find(el => el.getId() == id) != null
    }

    getStorageById(id) {
        return this.getStorage().find(el => el.getId() == id)
    }

    getAllTemplateStorage() {
        return this._storage.filter(el => el.isTemplateStorage() == true)
    }

    getAllOSStorage() {
        return this._storage.filter(el => el.isOSStorage() == true)
    }
}

module.exports = PVENode;