class VMTemplate {
    constructor(id, name, location) {
        this._id = id;
        this._name = name;
        this._location = location;
    }

    getId() {
        return this._id;
    }

    getName() {
        return this._name;
    }

    getLocation() {
        return this._location;
    }
}

module.exports = VMTemplate;