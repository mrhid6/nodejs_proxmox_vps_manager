class IPAddress {
    constructor(id, value, cidr, default_gateway, assigned_vm_id) {
        this._id = id;
        this._value = value;
        this._cidr = cidr;
        this._default_gateway = default_gateway;
        this._assigned_vm_id = assigned_vm_id;
    }

    getId() {
        return this._id;
    }

    getValue() {
        return this._value;
    }

    getCidr() {
        return this._cidr;
    }

    getDefaultGateway() {
        return this._default_gateway;
    }

    getAssignedVMId() {
        return this._assigned_vm_id;
    }

    isAssigned() {
        return this.getAssignedVMId() != null;
    }
}

module.exports = IPAddress;