class VMPackage {
    constructor(vm_id, type, name, template, ipaddress, storage, resource_plan, client) {
        this._vmid = vm_id;
        this._type = type;
        this._name = name;
        this._template = template;
        this._ipaddress = ipaddress;
        this._storage = storage;
        this._resource_plan = resource_plan;
        this._client = client;
    }

    getVMId() {
        return this._vmid;
    }

    getType() {
        return this._type;
    }

    getName() {
        return this._name;
    }

    getTemplate() {
        return this._template;
    }

    getIPAddress() {
        return this._ipaddress;
    }

    getStorage() {
        return this._storage;
    }

    getResourcePlan() {
        return this._resource_plan;
    }

    getClient() {
        return this._client;
    }
}

module.exports = VMPackage