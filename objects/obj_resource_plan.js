class ResourcePlan {
    constructor(id, name, type, cpulimit, ramlimit, diskspace) {
        this._id = id;
        this._name = name;
        this._type = type;
        this._cpulimit = cpulimit;
        this._ramlimit = ramlimit;
        this._diskspace = diskspace
    }

    getId() {
        return this._id;
    }

    getName() {
        return this._name;
    }

    getType() {
        return this._type;
    }

    getTypeString() {
        return this.getType() == 0 ? "LXC" : "QEMU";
    }

    getCpuLimit() {
        return this._cpulimit;
    }

    getRamLimit() {
        return this._ramlimit
    }

    getDiskSpace() {
        return this._diskspace;
    }
}

module.exports = ResourcePlan;