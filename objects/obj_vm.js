class VM {
    constructor(vmid, hostname, type, resource_plan, ipaddress, client_id) {
        this._vmid = vmid;
        this._hostname = hostname;
        this._type = type;
        this._resource_plan = resource_plan;
        this._ipaddress = ipaddress;
        this._client = client_id;
    }

    getVMId() {
        return this._vmid;
    }

    getHostname() {
        return this._hostname;
    }

    getType() {
        return this._type;
    }

    getTypeString() {
        return this.getType() == 0 ? "LXC" : "QEMU";
    }

    getResourcePlan() {
        return this._resource_plan
    }

    setResourcePlan(ResourcePlan) {
        this._resource_plan = ResourcePlan;
    }

    getIpAddress() {
        return this._ipaddress;
    }

    setIpAddress(IpAddress) {
        this._ipaddress = IpAddress;
    }

    getClient() {
        return this._client;
    }
}

module.exports = VM;