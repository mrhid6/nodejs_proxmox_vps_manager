const Logger = require("./logger");
const NET = require("./network");

const VPSManager = require("./vps_manager");

String.prototype.startsWith = function (prefix) {
    return this.indexOf(prefix) === 0;
};

String.prototype.endsWith = function (suffix) {
    return this.match(suffix + "$") == suffix;
};

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

Date.prototype.nth = function () {
    const d = this.getDate();
    if (d > 3 && d < 21) return 'th'; // thanks kennebec
    switch (d % 10) {
        case 1:
            return "st";
        case 2:
            return "nd";
        case 3:
            return "rd";
        default:
            return "th";
    }
}

Date.prototype.getMonthName = function () {
    const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
    ];
    return monthNames[this.getMonth()];
}

Number.prototype.pad = function (width, z) {
    let n = this;
    z = z || '0';
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

Number.prototype.toDecimal = function () {
    return (this / 100).toFixed(2);
}


const App = {}

App.init = function () {
    Logger.init();
    Logger.debug("Connecting to server..");
    NET.init();
    App.waitForNETConnect();
}

App.waitForNETConnect = function () {
    let interval = setInterval(() => {
        if (NET._connected == true) {
            Logger.success("Connected to server!");
            App.postInit();
            clearInterval(interval);
        }
    }, 100);
}

App.postInit = function () {
    VPSManager.init();
}

$(document).ready(function () {
    App.init();
});




module.exports = App;