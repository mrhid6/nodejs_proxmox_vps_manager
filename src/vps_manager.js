const Logger = require("./logger");
const NET = require("./network");
const Tools = require("../Mrhid6Utils/lib/tools");

// Objects
const iPVENode = require("../objects/obj_pve_node");
const iPVEStorage = require("../objects/obj_pve_storage");
const iResourcePlan = require("../objects/obj_resource_plan");
const iIpAddress = require("../objects/obj_ip_address");
const iVM = require("../objects/obj_vm");
const iVMTemplate = require("../objects/obj_vm_template");
const iClient = require("../objects/obj_client");

class VPSManager {
    constructor() {
        this.loaded = {
            pve_nodes: false,
            resource_plans: false,
            ip_address: false,
            vms: false,
            vm_templates: false,
            clients: false
        }
    }

    init() {

        Tools.declareSerializedClasses(
            iPVENode,
            iPVEStorage,
            iResourcePlan,
            iIpAddress,
            iVM,
            iVMTemplate,
            iClient
        )

        this.setupEventHandler();
        this.setupJqueryEventHandlers();
        this.requestPVENodes();
        this.requestResourcePlans();
        this.requestVirtualMachines();
        this.requestIPAddresses();
        this.requestVMTemplates();
        this.requestClients();
    }

    setupEventHandler() {
        NET.addEventHandler("packet.server.get.pvenodes.res", Packet => {
            this.handlePVENodesPacket(Packet);
        });

        NET.addEventHandler("packet.server.get.resourceplans.res", Packet => {
            this.handleResourcePlansPacket(Packet);
        });

        NET.addEventHandler("packet.server.get.ipaddresses.res", Packet => {
            this.handleIpAddressesPacket(Packet);
        });

        NET.addEventHandler("packet.server.get.vms.res", Packet => {
            this.handleVirtualMachinesPacket(Packet);
        });

        NET.addEventHandler("packet.server.get.pve.vms.res", Packet => {
            this.handlePVEVirtualMachinesPacket(Packet);
        });

        NET.addEventHandler("packet.server.get.vmtemplates.res", Packet => {
            this.handleVMTemplatesPacket(Packet);
        });

        NET.addEventHandler("packet.server.get.clients.res", Packet => {
            this.handleClientsPacket(Packet);
        });

        NET.addEventHandler("packet.server.add.resourceplan.res", Packet => {
            this.closeAddResourcePlanModal(Packet);
        });

        NET.addEventHandler("packet.server.add.iprange.res", Packet => {
            this.closeAddIpRangeModal(Packet);
        });

        NET.addEventHandler("packet.server.new.vm.res", Packet => {
            this.handleNewVMResponcePacket(Packet);
        });
    }

    setupJqueryEventHandlers() {
        this.waitTilFullyLoaded().then(() => {
            this.setupNewVMWizard()
        })

        $(".sidebar-dropdown > a").click(function () {
            $(".sidebar-submenu").slideUp(150);
            if (
                $(this)
                .parent()
                .hasClass("active")
            ) {
                $(".sidebar-dropdown").removeClass("active");
                $(this)
                    .parent()
                    .removeClass("active");
            } else {
                $(".sidebar-dropdown").removeClass("active");
                $(this)
                    .next(".sidebar-submenu")
                    .slideDown(150);
                $(this)
                    .parent()
                    .addClass("active");
            }
        });

        $("#close-sidebar").click(function () {
            $(".page-wrapper").removeClass("toggled");
        });
        $("#show-sidebar").click(function () {
            $(".page-wrapper").addClass("toggled");
        });

        $("body").on("click", "#btn-rescan-nodes", (e) => {
            this.rescanPVENodes();
        }).on("click", "#btn-rescan-storage", (e) => {
            this.rescanPVEStorage();
        }).on("click", "#btn-add-res-plan", (e) => {
            this.openAddResourcePlanModal();
        }).on("click", "#btn-add-res-plan-form", (e) => {
            this.submitAddResourcePlanForm(e);
        }).on("change", "#inp-add-res-plan-cpu", (e) => {
            const $self = $(e.currentTarget)

            if ($self.val() > 64) {
                $self.val(64)
            } else if ($self.val() < 1) {
                $self.val(1)
            }
        }).on("change", "#inp-add-res-plan-ram", (e) => {
            const $self = $(e.currentTarget)

            if ($self.val() > 64) {
                $self.val(64)
            } else if ($self.val() < 1) {
                $self.val(1)
            }
        }).on("change", "#inp-add-res-plan-disk", (e) => {
            const $self = $(e.currentTarget)

            if ($self.val() > 200) {
                $self.val(200)
            } else if ($self.val() < 20) {
                $self.val(20)
            }
        }).on("click", "#btn-add-ip-range", (e) => {
            this.openAddIpRangeModal();
        }).on("click", "#btn-add-ip-range-form", (e) => {
            this.submitAddIpRangeForm(e);
        }).on("click", "#btn-listvms-rescan", (e) => {
            this.openRescanPVEVMsModal();
        }).on("click", "#btn-show-pve-vms", (e) => {
            this.requestPVEVMs(e);
        }).on("click", "#btn-rescan-templates", (e) => {
            this.rescanPVEVMTemplates();
        }).on("click", "#btn-new-vm-generate-hostname", (e) => {
            e.preventDefault();
            const $inp_hostname = $("#inp-new-vm-name")

            const randomNum = Math.floor(Math.random() * Math.floor(100000000)).pad(8)
            $inp_hostname.val("atom-" + randomNum);
        }).on("change", "#sel-new-vm-resplan", () => {
            this.changeNewVMResourcePlanInfo();
        })
    }

    isLoaded() {
        return (
            this.loaded.pve_nodes == true &&
            this.loaded.resource_plans == true &&
            this.loaded.ip_address == true &&
            this.loaded.vms == true &&
            this.loaded.vm_templates == true &&
            this.loaded.clients == true
        )
    }

    waitTilFullyLoaded() {
        return new Promise((resolve, reject) => {
            const interval = setInterval(() => {
                if (this.isLoaded() == true) {
                    resolve();
                    clearInterval(interval);
                }
            }, 100)
        })
    }

    requestPVENodes() {
        const Packet = NET.createPrefixedPacket("get.pvenodes");
        NET.sendClientPacket(Packet);
    }

    requestResourcePlans() {
        const Packet = NET.createPrefixedPacket("get.resourceplans");
        NET.sendClientPacket(Packet);
    }

    requestIPAddresses() {
        const Packet = NET.createPrefixedPacket("get.ipaddresses");
        NET.sendClientPacket(Packet);
    }

    requestVirtualMachines() {
        const Packet = NET.createPrefixedPacket("get.vms");
        NET.sendClientPacket(Packet);
    }

    requestPVEVMs(e) {
        const $self = $(e.currentTarget);
        $self.append(" <i class='fas fa-sync fa-spin'></i>")
        $self.prop("disabled", true)
        const Packet = NET.createPrefixedPacket("get.pve.vms");
        NET.sendClientPacket(Packet);
    }

    requestVMTemplates() {
        const Packet = NET.createPrefixedPacket("get.vmtemplates");
        NET.sendClientPacket(Packet);
    }

    requestClients() {
        const Packet = NET.createPrefixedPacket("get.clients");
        NET.sendClientPacket(Packet);
    }


    handlePVENodesPacket(Packet) {
        this.PVENodes = Tools.deserialize(Packet.getPacketData());
        this.loaded.pve_nodes = true;

        console.log(this.PVENodes);
        this.displayPVENodesTable();
        this.displayPVEStorageTable();
    }

    handleResourcePlansPacket(Packet) {
        this.ResourcePlans = Tools.deserialize(Packet.getPacketData());
        this.loaded.resource_plans = true;
        console.log(this.ResourcePlans);
        this.displayResourcePlansTable();
    }

    handleIpAddressesPacket(Packet) {
        this.IpAddresses = Tools.deserialize(Packet.getPacketData());
        this.loaded.ip_address = true;
        console.log(this.IpAddresses);
        this.displayIpAddressesTable();
    }

    handleVirtualMachinesPacket(Packet) {
        this.VMs = Tools.deserialize(Packet.getPacketData());
        this.loaded.vms = true;
        console.log(this.VMs);
        this.displayVirtualMachineTable();
    }

    handlePVEVirtualMachinesPacket(Packet) {
        const PVEVMs = Tools.deserialize(Packet.getPacketData());
        console.log(PVEVMs);
        $("#btn-show-pve-vms").prop("disabled", false);
        $("#btn-show-pve-vms i").remove();
        this.updateRescanPVEVMsTable(PVEVMs);
    }

    handleVMTemplatesPacket(Packet) {
        this.VMTemplates = Tools.deserialize(Packet.getPacketData());
        this.loaded.vm_templates = true;
        console.log(this.VMTemplates);
        this.displayVMTemplatesTable();
    }

    handleClientsPacket(Packet) {
        this.Clients = Tools.deserialize(Packet.getPacketData());
        this.loaded.clients = true;
        console.log(this.Clients);
        this.displayClientsTable()
    }

    handleNewVMResponcePacket(Packet) {
        const pData = Packet.getPacketData();

        if (pData.result == "error") {
            $(".pending-vm-creation").hide()
            $(".vm-creation-failed").show()

            $(".vm-creation-failed error_msg").text(pData.error_msg);
            return;
        } else if (pData.result == "success") {
            $(".pending-vm-creation").hide()
            $(".vm-creation-completed").show()
        }
    }

    getPVEStorageById(id) {
        const PVENode = this.PVENodes.find(el => el.hasStorageWithId(id))
        if (PVENode == null) return null;
        return PVENode.getStorageById(id);
    }

    displayPVENodesTable() {
        if ($("#pvenodes-table").length == 0) return;

        const data = this.PVENodesTableData();

        const isDataTable = $.fn.dataTable.isDataTable("#pvenodes-table")
        if (isDataTable == false) {

            $("#pvenodes-table").DataTable({
                paging: false,
                searching: false,
                info: false,
                order: [
                    [0, "asc"]
                ],
                columnDefs: [{
                    "targets": 2,
                    "orderable": false
                }],
                data: data,
            })
        } else {
            this.updatePVENodesTable();
        }
    }

    updatePVENodesTable() {
        const data = this.PVENodesTableData();
        const datatable = $("#pvenodes-table").DataTable();
        datatable.clear();
        datatable.rows.add(data);
        datatable.draw();
    }

    PVENodesTableData() {
        const data = [];
        const nodes = this.PVENodes;


        for (let i = 0; i < nodes.length; i++) {
            const node = nodes[i];

            const row = [
                node.getId(),
                node.getName(),
                node.getStatus()
            ]

            data.push(row);
        }

        return data;
    }

    displayPVEStorageTable() {
        if ($("#pvestorage1-table").length == 0) return;
        const data = this.PVEStorageTableData()
        const data1 = data[0];
        const data2 = data[1];

        const isDataTable = $.fn.dataTable.isDataTable("#pvestorage1-table")
        if (isDataTable == false) {

            $("#pvestorage1-table").DataTable({
                paging: false,
                searching: false,
                info: false,
                order: [
                    [0, "asc"]
                ],
                data: data1,
            })
            $("#pvestorage2-table").DataTable({
                paging: false,
                searching: false,
                info: false,
                order: [
                    [0, "asc"]
                ],
                data: data2,
            })
        } else {
            this.updatePVEStorageTable();
        }
    }

    updatePVEStorageTable() {
        const data = this.PVEStorageTableData()
        const data1 = data[0];
        const data2 = data[1];

        const datatable1 = $("#pvestorage1-table").DataTable();
        datatable1.clear();
        datatable1.rows.add(data1);
        datatable1.draw();
        const datatable2 = $("#pvestorage2-table").DataTable();
        datatable2.clear();
        datatable2.rows.add(data2);
        datatable2.draw();
    }

    PVEStorageTableData() {
        const data = [
            [],
            []
        ];
        const nodes = this.PVENodes;

        const sharedStorageNames = [];

        for (let i = 0; i < nodes.length; i++) {
            const PVENode = nodes[i];
            const storage = PVENode.getStorage();
            for (let j = 0; j < storage.length; j++) {
                const PVEStorage = storage[j];



                if (PVEStorage.isShared() == true) {
                    if (sharedStorageNames.indexOf(PVEStorage.getName()) > -1) {
                        continue;
                    }

                    sharedStorageNames.push(PVEStorage.getName())
                    const row = [
                        PVEStorage.getId(),
                        PVEStorage.getName(),
                        PVEStorage.getAllowedContent().join(", ")
                    ]
                    data[0].push(row);
                } else {
                    const row = [
                        PVEStorage.getId(),
                        PVEStorage.getName(),
                        PVENode.getName(),
                        PVEStorage.getAllowedContent().join(", ")
                    ]
                    data[1].push(row);
                }
            }
        }

        return data;
    }

    rescanPVENodes() {
        const Packet = NET.createPrefixedPacket("rescan.pvenodes");
        NET.sendClientPacket(Packet);
    }

    rescanPVEStorage() {
        const Packet = NET.createPrefixedPacket("rescan.pvestorage");
        NET.sendClientPacket(Packet);
    }

    rescanPVEVMTemplates() {
        const Packet = NET.createPrefixedPacket("rescan.pvetemplates");
        NET.sendClientPacket(Packet);
    }

    displayResourcePlansTable() {
        if ($("#resourceplans-table").length == 0) return;

        const data = this.ResourcePlansTableData();

        const isDataTable = $.fn.dataTable.isDataTable("#resourceplans-table")
        if (isDataTable == false) {

            $("#resourceplans-table").DataTable({
                paging: false,
                searching: false,
                info: false,
                order: [
                    [0, "asc"]
                ],
                columnDefs: [{
                    "targets": 2,
                    "orderable": false
                }],
                data: data,
            })
        } else {
            this.updateResourcePlansTable();
        }
    }

    updateResourcePlansTable() {
        const data = this.ResourcePlansTableData();
        const datatable = $("#resourceplans-table").DataTable();
        datatable.clear();
        datatable.rows.add(data);
        datatable.draw();
    }

    ResourcePlansTableData() {
        const data = [];
        const ResourcePlans = this.ResourcePlans;


        for (let i = 0; i < ResourcePlans.length; i++) {
            const ResourcePlan = ResourcePlans[i];

            const row = [
                ResourcePlan.getId(),
                ResourcePlan.getName(),
                ResourcePlan.getTypeString(),
                ResourcePlan.getCpuLimit(),
                bytesToSize(ResourcePlan.getRamLimit()),
                ResourcePlan.getDiskSpace() + "GB"
            ]

            data.push(row);
        }

        return data;
    }

    displayIpAddressesTable() {
        if ($("#ipaddresses-table").length == 0) return;

        const data = this.IpAddressesData();

        const isDataTable = $.fn.dataTable.isDataTable("#ipaddresses-table")
        if (isDataTable == false) {

            $("#ipaddresses-table").DataTable({
                paging: true,
                searching: false,
                info: false,
                order: [
                    [1, "asc"]
                ],
                data: data,
            })
        } else {
            this.updateIpAddressesTable();
        }
    }

    updateIpAddressesTable() {
        const data = this.IpAddressesData();
        const datatable = $("#ipaddresses-table").DataTable();
        datatable.clear();
        datatable.rows.add(data);
        datatable.draw();
    }

    IpAddressesData() {
        const data = [];
        const IpAddresses = this.IpAddresses;


        for (let i = 0; i < IpAddresses.length; i++) {
            const IpAddress = IpAddresses[i];

            let VMName = "Not Assigned"

            if (IpAddress.getAssignedVMId() != null) {
                const VM = this.VMs.find(el => el.getVMId() == IpAddress.getAssignedVMId())

                if (VM != null) {
                    VMName = VM.getHostname();
                }
            }

            const row = [
                IpAddress.getId(),
                IpAddress.getValue() + IpAddress.getCidr(),
                VMName
            ]

            data.push(row);
        }

        return data;
    }

    displayVirtualMachineTable() {
        if ($("#vms-table").length == 0) return;

        const data = this.VirtualMachineData();

        const isDataTable = $.fn.dataTable.isDataTable("#vms-table")
        if (isDataTable == false) {

            $("#vms-table").DataTable({
                paging: true,
                searching: false,
                info: false,
                order: [
                    [1, "asc"]
                ],
                data: data,
            })
        } else {
            this.updateVirtualMachineTable();
        }
    }

    updateVirtualMachineTable() {
        const data = this.VirtualMachineData();
        const datatable = $("#vms-table").DataTable();
        datatable.clear();
        datatable.rows.add(data);
        datatable.draw();
    }

    VirtualMachineData() {
        const data = [];
        const VMs = this.VMs;


        for (let i = 0; i < VMs.length; i++) {
            const VM = VMs[i];

            const row = [
                VM.getVMId(),
                VM.getHostname(),
                VM.getTypeString(),
                VM.getResourcePlan().getName(),
                VM.getIpAddress().getValue(),
                VM.getClient()
            ]

            data.push(row);
        }

        return data;
    }

    displayVMTemplatesTable() {
        if ($("#vm-templates-table").length == 0) return;

        const data = this.VMTemplateTableData();

        const isDataTable = $.fn.dataTable.isDataTable("#vm-templates-table")
        if (isDataTable == false) {

            $("#vm-templates-table").DataTable({
                paging: true,
                searching: false,
                info: false,
                order: [
                    [0, "asc"]
                ],
                data: data,
            })
        } else {
            this.updateVMTemplatesTable();
        }
    }

    updateVMTemplatesTable() {
        const data = this.VMTemplateTableData();
        const datatable = $("#vm-templates-table").DataTable();
        datatable.clear();
        datatable.rows.add(data);
        datatable.draw();
    }

    VMTemplateTableData() {
        const data = [];
        const VMTemplates = this.VMTemplates;


        for (let i = 0; i < VMTemplates.length; i++) {
            const Template = VMTemplates[i];

            const row = [
                Template.getId(),
                Template.getName()
            ]

            data.push(row);
        }

        return data;
    }

    displayClientsTable() {
        if ($("#clients-table").length == 0) return;

        const data = this.ClientsTableData();

        const isDataTable = $.fn.dataTable.isDataTable("#clients-table")
        if (isDataTable == false) {

            $("#clients-table").DataTable({
                paging: true,
                searching: false,
                info: false,
                order: [
                    [0, "asc"]
                ],
                data: data,
            })
        } else {
            this.updateClientsTable();
        }
    }

    updateClientsTable() {
        const data = this.ClientsTableData();
        const datatable = $("#clients-table").DataTable();
        datatable.clear();
        datatable.rows.add(data);
        datatable.draw();
    }

    ClientsTableData() {
        const data = [];
        const Clients = this.Clients;


        for (let i = 0; i < Clients.length; i++) {
            const Client = Clients[i];

            const row = [
                Client.getId(),
                Client.getFullName(),
                Client.getEmail()
            ]

            data.push(row);
        }

        return data;
    }

    openAddResourcePlanModal() {
        if (Tools.modal_opened == true) return;

        Tools.openModal("add-resource-plan", modal => {

        })
    }

    submitAddResourcePlanForm(e) {
        e.preventDefault();
        console.log("Clicked!");
        $(".modal .error").removeClass("alert alert-danger").empty();

        const $inp_name = $("#inp-add-res-plan-name")
        const $sel_type = $("#sel-add-res-plan-type")
        const $inp_cpu = $("#inp-add-res-plan-cpu")
        const $inp_ram = $("#inp-add-res-plan-ram")
        const $inp_disk = $("#inp-add-res-plan-disk")

        if ($inp_name.val() == "" || $sel_type.val() == "-1") {
            $(".modal .error").addClass("alert alert-danger").text("Please fill in all required fields!")
            return;
        }

        if (isNaN($inp_cpu.val()) == true) {
            $(".modal .error").addClass("alert alert-danger").text("CPU amount is not a number!")
            return;
        }

        if (isNaN($inp_ram.val()) == true) {
            $(".modal .error").addClass("alert alert-danger").text("RAM amount is not a number!")
            return;
        }

        if (isNaN($inp_disk.val()) == true) {
            $(".modal .error").addClass("alert alert-danger").text("Disk Space is not a number!")
            return;
        }

        const pData = {
            name: $inp_name.val(),
            type: $sel_type.val(),
            cpu: parseInt($inp_cpu.val()),
            ram: (parseInt($inp_ram.val()) * 1024),
            disk: parseInt($inp_disk.val())
        }

        const Packet = NET.createPrefixedPacket("add.resourceplan", pData);
        NET.sendClientPacket(Packet);
    }

    closeAddResourcePlanModal(Packet) {
        const pData = Packet.getPacketData();
        if (pData.result != "success") {
            $(".modal#add-resource-plan .error").addClass("alert alert-danger").text("Server error: could not create resouce plan!")
            return;
        }
        $(".modal#add-resource-plan button.close").trigger("click");
    }

    openAddIpRangeModal() {
        if (Tools.modal_opened == true) return;

        Tools.openModal("add-ip-range", modal => {})
    }

    submitAddIpRangeForm(e) {
        e.preventDefault();
        console.log("Clicked!");
        $(".modal .error").removeClass("alert alert-danger").empty();

        const $inp_start = $("#inp-add-ip-range-start")
        const $inp_end = $("#inp-add-ip-range-end")
        const $inp_defgw = $("#inp-add-ip-range-default-gw")
        const $sel_cidr = $("#sel-add-ip-range-cidr")

        if ($inp_start.val() == "" || $inp_end.val() == "" || $sel_cidr.val() == "-1" || $inp_defgw.val() == "") {
            $(".modal .error").addClass("alert alert-danger").text("Please fill in all required fields!")
            return;
        }

        if (ValidateIPaddress($inp_start.val()) == false) {
            $(".modal .error").addClass("alert alert-danger").text("Starting IP Address is not a valid IP Address!")
            return;
        }

        if (ValidateIPaddress($inp_end.val()) == false) {
            $(".modal .error").addClass("alert alert-danger").text("Ending IP Address is not a valid IP Address!")
            return;
        }

        if (ValidateIPaddress($inp_defgw.val()) == false) {
            $(".modal .error").addClass("alert alert-danger").text("Default Gateway IP Address is not a valid IP Address!")
            return;
        }

        const pData = {
            start: $inp_start.val(),
            end: $inp_end.val(),
            cidr: $sel_cidr.val(),
            defgw: $inp_defgw.val()
        }

        const Packet = NET.createPrefixedPacket("add.iprange", pData);
        NET.sendClientPacket(Packet);
    }

    closeAddIpRangeModal(Packet) {
        const pData = Packet.getPacketData();
        if (pData.result != "success") {
            $(".modal#add-ip-range .error").addClass("alert alert-danger").text("Server error: " + pData.error)
            return;
        }
        $(".modal#add-ip-range button.close").trigger("click");
    }

    openRescanPVEVMsModal() {
        if (Tools.modal_opened == true) return;

        Tools.openModal("rescan-pve-vms", modal => {
            $("#rescanned-vms-table").DataTable({
                paging: true,
                searching: false,
                info: false,
                'columnDefs': [{
                    'targets': 0,
                    'checkboxes': {
                        'selectRow': true
                    }
                }],
                'select': {
                    'style': 'multi'
                },
                order: [
                    [1, "asc"]
                ],
                data: [],
            })
        })
    }

    updateRescanPVEVMsTable(VMs) {
        const data = this.RescanPVEVMsTableData(VMs);
        const datatable = $("#rescanned-vms-table").DataTable();
        datatable.clear();
        datatable.rows.add(data);
        datatable.draw();
    }

    RescanPVEVMsTableData(VMs) {
        const data = [];

        for (let i = 0; i < VMs.length; i++) {
            const VM = VMs[i];

            const existVm = this.VMs.find(el => el.getVMId() == VM.getVMId())
            if (existVm != null)
                continue;

            const row = [
                "",
                VM.getVMId(),
                VM.getHostname(),
                VM.getResourcePlan().getName(),
                VM.getIpAddress().getValue(),
                VM.getClient()
            ]

            data.push(row);
        }

        return data;
    }

    submitRescanPVEVMsForm(e) {
        e.preventDefault();
        console.log("Clicked!");
        $(".modal .error").removeClass("alert alert-danger").empty();

        const $inp_start = $("#inp-add-ip-range-start")
        const $inp_end = $("#inp-add-ip-range-end")

        if ($inp_start.val() == "" || $inp_end.val() == "") {
            $(".modal .error").addClass("alert alert-danger").text("Please fill in all required fields!")
            return;
        }

        if (ValidateIPaddress($inp_start.val()) == false) {
            $(".modal .error").addClass("alert alert-danger").text("Starting IP Address is not a valid IP Address!")
            return;
        }

        if (ValidateIPaddress($inp_end.val()) == false) {
            $(".modal .error").addClass("alert alert-danger").text("Ending IP Address is not a valid IP Address!")
            return;
        }

        const pData = {
            start: $inp_start.val(),
            end: $inp_end.val(),
        }

        const Packet = NET.createPrefixedPacket("add.iprange", pData);
        NET.sendClientPacket(Packet);
    }

    closeRescanPVEVMsModal(Packet) {
        const pData = Packet.getPacketData();
        if (pData.result != "success") {
            $(".modal#add-ip-range .error").addClass("alert alert-danger").text("Server error: " + pData.error)
            return;
        }
        $(".modal#add-ip-range button.close").trigger("click");
    }

    setupNewVMWizard() {
        if ($("#new-vm-wizard").length == 0)
            return

        $("#new-vm-wizard").steps({
            onStepChanging: (event, currentIndex, nextIndex) => {

                if (nextIndex < currentIndex || currentIndex == 4) {
                    return true;
                }

                if (nextIndex == 4) {
                    let summary_type = $("#sel-new-vm-type option:selected").text();
                    let summary_name = $("#inp-new-vm-name").val();
                    let summary_template = $("#sel-new-vm-template option:selected").text();
                    let summary_ipaddress = $("#sel-new-vm-ipaddress option:selected").text();
                    const sel_ip_id = $("#sel-new-vm-ipaddress").val()
                    const IPAddress = this.IpAddresses.find(el => el.getId() == sel_ip_id)
                    if (IPAddress == null)
                        return false;

                    const sel_storage_id = $("#sel-new-vm-storage").val()
                    const PVEStorage = this.getPVEStorageById(sel_storage_id)
                    console.log(PVEStorage)
                    if (PVEStorage == null)
                        return false;

                    let summary_resplan = $("#sel-new-vm-resplan option:selected").text();
                    const sel_resplan_id = $("#sel-new-vm-resplan").val()
                    const ResourcePlan = this.ResourcePlans.find(el => el.getId() == sel_resplan_id)
                    if (ResourcePlan == null)
                        return false;

                    let summary_client = $("#sel-new-vm-client option:selected").text();

                    $("#new-vm-summary-type b").text(summary_type);
                    $("#new-vm-summary-name b").text(summary_name);
                    $("#new-vm-summary-template b").text(summary_template);
                    $("#new-vm-summary-ipaddress b").text(summary_ipaddress);
                    $("#new-vm-summary-ipaddress-ip b").text(IPAddress.getValue());
                    $("#new-vm-summary-ipaddress-defaultgw b").text(IPAddress.getDefaultGateway());
                    $("#new-vm-summary-ipaddress-cidr b").text(IPAddress.getCidr());
                    $("#new-vm-summary-storage b").text(PVEStorage.getName());
                    $("#new-vm-summary-resplan b").text(summary_resplan);
                    $("#new-vm-summary-resplan-cpu b").text(ResourcePlan.getCpuLimit());
                    $("#new-vm-summary-resplan-ram b").text(bytesToSize(ResourcePlan.getRamLimit()));
                    $("#new-vm-summary-resplan-disk b").text(ResourcePlan.getDiskSpace() + " GB");
                    $("#new-vm-summary-client b").text(summary_client);
                }

                if (currentIndex == 0) {
                    return ($("#sel-new-vm-type").val() != "-1")

                } else if (currentIndex == 1) {
                    if ($("#inp-new-vm-name").val() == "") {
                        return false;
                    }

                    if ($("#sel-new-vm-template").val() == "-1") {
                        return false;
                    }

                    if ($("#sel-new-vm-ipaddress").val() == "-1") {
                        return false;
                    }

                    if ($("#sel-new-vm-storage").val() == "-1") {
                        return false;
                    }

                    return true;
                } else if (currentIndex == 2) {
                    if ($("#sel-new-vm-resplan").val() == "-1") {
                        return false;
                    }

                    return true;
                } else if (currentIndex == 3) {
                    if ($("#sel-new-vm-client").val() == "-1") {
                        return false;
                    }

                    return true;
                }
            },
            onFinishing: (event, currentIndex) => {
                if ($("#sel-new-vm-type").val() == "-1") {
                    return false;
                }

                if ($("#inp-new-vm-name").val() == "") {
                    return false;
                }

                if ($("#sel-new-vm-template").val() == "-1") {
                    return false;
                }

                if ($("#sel-new-vm-ipaddress").val() == "-1") {
                    return false;
                }
                if ($("#sel-new-vm-storage").val() == "-1") {
                    return false;
                }
                if ($("#sel-new-vm-resplan").val() == "-1") {
                    return false;
                }
                if ($("#sel-new-vm-client").val() == "-1") {
                    return false;
                }
                return true;
            },
            onFinished: () => {
                this.submitNewVMWizard();
            }
        });

        $("#new-vm-wizard").show();

        this.fillNewVMWizard();
    }

    fillNewVMWizard() {
        const $sel_templates = $("#sel-new-vm-template")
        const $sel_ipaddress = $("#sel-new-vm-ipaddress")
        const $sel_storage = $("#sel-new-vm-storage")
        const $sel_resplan = $("#sel-new-vm-resplan")
        const $sel_clients = $("#sel-new-vm-client")

        for (let i = 0; i < this.VMTemplates.length; i++) {
            const VMTemplate = this.VMTemplates[i];
            $sel_templates.append("<option value='" + VMTemplate.getId() + "'>" + VMTemplate.getName() + "</option>")
        }

        for (let i = 0; i < this.IpAddresses.length; i++) {
            const IpAddress = this.IpAddresses[i];
            const ipName = IpAddress.getValue() + IpAddress.getCidr();
            let disabled = "";

            if (IpAddress.getAssignedVMId() != null) {
                disabled = "disabled"
            }

            $sel_ipaddress.append("<option " + disabled + " value='" + IpAddress.getId() + "'>" + ipName + "</option>")
        }

        for (let i = 0; i < this.ResourcePlans.length; i++) {
            const ResourcePlan = this.ResourcePlans[i];
            const planName = ResourcePlan.getName();
            $sel_resplan.append("<option value='" + ResourcePlan.getId() + "'>" + planName + "</option>")
        }

        for (let i = 0; i < this.Clients.length; i++) {
            const Client = this.Clients[i];
            const client_name = Client.getFullName();
            $sel_clients.append("<option value='" + Client.getId() + "'>" + client_name + "</option>")
        }

        const shared_storage = [];

        for (let i = 0; i < this.PVENodes.length; i++) {
            const PVENode = this.PVENodes[i];

            const storages = PVENode.getAllOSStorage();
            for (let j = 0; j < storages.length; j++) {
                const store = storages[j];
                if (store.isShared()) {
                    const exists = shared_storage.find(el => el.getName() == store.getName())
                    if (exists != null) continue;

                    shared_storage.push(store);
                }
            }
        }

        for (let i = 0; i < shared_storage.length; i++) {
            const store = shared_storage[i];
            $sel_storage.append("<option value='" + store.getId() + "'>" + store.getName() + "</option>")
        }
    }

    changeNewVMResourcePlanInfo() {
        const $sel_resplan = $("#sel-new-vm-resplan");
        const ResourcePlan = this.ResourcePlans.find(el => el.getId() == $sel_resplan.val())
        if (ResourcePlan == null) return;

        $("#inp-new-vm-resplan-type").val(ResourcePlan.getTypeString())
        $("#inp-new-vm-resplan-cpus").val(ResourcePlan.getCpuLimit())
        $("#inp-new-vm-resplan-ram").val(bytesToSize(ResourcePlan.getRamLimit()))
        $("#inp-new-vm-resplan-diskspace").val(ResourcePlan.getDiskSpace() + " GB")
    }

    submitNewVMWizard() {
        const vm_type = $("#sel-new-vm-type").val()
        const vm_name = $("#inp-new-vm-name").val()
        const vm_template = $("#sel-new-vm-template").val()
        const vm_ip = $("#sel-new-vm-ipaddress").val()
        const vm_storage = $("#sel-new-vm-storage").val()
        const vm_resplan = $("#sel-new-vm-resplan").val()
        const vm_client = $("#sel-new-vm-client").val()
        const pData = {
            vm_type,
            vm_name,
            vm_template,
            vm_ip,
            vm_storage,
            vm_resplan,
            vm_client
        }
        $("#new-vm-wizard").hide()
        $(".pending-vm-creation").show();

        const Packet = NET.createPrefixedPacket("new.vm", pData);
        NET.sendClientPacket(Packet);
    }
}

function bytesToSize(bytes) {
    var sizes = ['MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
};

function ValidateIPaddress(ipaddress) {
    var pattern = /\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/;

    if (pattern.test(ipaddress)) {
        return true;
    }
    return false;
}

const vpsManager = new VPSManager();

module.exports = vpsManager;