const NET = new(require("../../Mrhid6Utils/lib/networkclient"));

NET.init = function () {
    NET.setupEventHandlers();
    NET._debugTriggeredEvents = true;
    NET.startClientConnection("/app", io);
}

NET.setupEventHandlers = function () {
    NET.addEventHandler("connect", function () {});
}

module.exports = NET;