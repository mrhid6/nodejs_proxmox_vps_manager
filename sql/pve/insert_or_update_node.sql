SET @nodename=?;
SET @nodestatus=?;

INSERT INTO pve_nodes SET
pve_node_name=@nodename,
pve_node_status=@nodestatus
ON DUPLICATE KEY UPDATE pve_node_status=@nodestatus;