SET @pvenodeid=?;
SET @storagename=?;
SET @shared=?;
SET @allowedcontent=?;

INSERT INTO pve_storage SET
pve_storage_node_id=@pvenodeid,
pve_storage_name=@storagename,
pve_storage_shared=@shared,
pve_storage_allowed_content=@allowedcontent;