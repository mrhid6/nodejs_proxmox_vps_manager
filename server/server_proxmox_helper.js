const ProxmoxController = require("./server_proxmox_controller");
var Cleanup = require("./server_cleanup");
var logger = require("./server_logger");

class PromoxHelper {
    constructor() {
        this.controllerLoaded = false;
    }

    init() {
        ProxmoxController.init().then(() => {
            this.controllerLoaded = true;
        }).catch(err => {
            this.controllerLoaded = false;
            logger.error("Error: " + err)
        })
    }

    getNodes() {
        return new Promise((resolve, reject) => {
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, "/nodes").then(nodes => {
                resolve(nodes)
            })
        })
    }

    getOnlineNodes() {
        return new Promise((resolve, reject) => {
            this.getNodes().then(PVENodes => {
                const online_nodes = [];

                for (let i = 0; i < PVENodes.length; i++) {
                    const node = PVENodes[i];
                    if (node.status == "online") {
                        online_nodes.push(node)
                    }
                }

                resolve(online_nodes);
            })
        })
    }

    getNode(nodeName) {
        return new Promise((resolve, reject) => {
            const path = "/nodes/" + nodeName
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, path).then(node => {
                resolve(node)
            })
        })
    }

    getRandomOnlineNode() {
        return new Promise((resolve, reject) => {
            this.getOnlineNodes().then(PVENodes => {
                var node = PVENodes[Math.floor(Math.random() * PVENodes.length)];
                resolve(node);
            })
        })
    }

    getClusterStorage() {
        return new Promise((resolve, reject) => {
            const path = "/cluster/resources?type=storage"
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, path).then(storage => {
                resolve(storage)
            })
        })
    }

    getVirtualMachines() {
        return new Promise((resolve, reject) => {

            const path = "/cluster/resources?type=vm"
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, path).then(vms => {
                resolve(vms)
            }).catch(err => {
                console.log(err)
            })
        })
    }

    getVirtualMachineInNode(node, id) {
        return new Promise((resolve, reject) => {

            const path = "/nodes/" + node + "/" + id + "/config"
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, path).then(storage => {
                resolve(storage)
            }).catch(err => {
                console.log(err)
            })
        });
    }

    getVirtualMachineTemplates(node, storage) {
        return new Promise((resolve, reject) => {

            const path = "/nodes/" + node + "/storage/" + storage + "/content?content=vztmpl"
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, path).then(templates => {
                resolve(templates)
            }).catch(err => {
                console.log(err)
            })
        });
    }

    getStorageFromNode(node, storage) {
        return new Promise((resolve, reject) => {

            const path = "/nodes/" + node + "/storage/" + storage + "/status"
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, path).then(storage => {
                resolve(storage)
            }).catch(err => {
                console.log(err)
            })
        })
    }

    getTaskStatus(upid) {
        return new Promise((resolve, reject) => {

            const nodeName = upid.split(":")[1];

            const path = "/nodes/" + nodeName + "/tasks/" + upid + "/status"
            ProxmoxController.makeRequest(ProxmoxController.RequestMethods.GET, path).then(status => {
                resolve(status)
            }).catch(err => {
                console.log(err)
            })

        });
    }

    waitForVMTaskCompleted(task_id) {
        return new Promise((resolve, reject) => {
            const interval = setInterval(() => {
                this.getTaskStatus(task_id).then((status) => {

                    if (status.status == "stopped") {
                        if (status.exitstatus == "OK") {
                            resolve();
                        } else {
                            console.log(status)
                            reject("Error")
                        }
                        clearInterval(interval);
                    }
                })
            }, 500)
        });
    }

    createVMFromVMPackage(VMPackage) {
        return new Promise((resolve, reject) => {
            if (VMPackage.getType() == 0) {
                this.createVMLXCFromVMPackage(VMPackage).then((task_id) => {
                    return this.waitForVMTaskCompleted(task_id)
                }).then(() => {
                    resolve();
                }).catch(err => {
                    reject(err)
                })
            }
        })
    }

    createVMLXCFromVMPackage(VMPackage) {
        return new Promise((resolve, reject) => {
            console.log("Create LXC!")
            const rootfs = "volume=" + VMPackage.getStorage().getName() + ":" + VMPackage.getResourcePlan().getDiskSpace();
            let network = "name=eth0,bridge=vmbr0"
            network += ",gw=" + VMPackage.getIPAddress().getDefaultGateway();
            network += ",ip=" + VMPackage.getIPAddress().getValue() + VMPackage.getIPAddress().getCidr()

            const description = {
                vmid: VMPackage.getVMId(),
                clientid: VMPackage.getClient().getId(),
                resource_plan: VMPackage.getResourcePlan().getId(),
                ipaddress: VMPackage.getIPAddress().getId()
            }

            const lxc_data = {
                vmid: VMPackage.getVMId(),
                ostemplate: VMPackage.getTemplate().getLocation(),
                storage: VMPackage.getStorage().getName(),
                hostname: VMPackage.getName(),
                cores: VMPackage.getResourcePlan().getCpuLimit(),
                memory: VMPackage.getResourcePlan().getRamLimit(),
                rootfs: rootfs,
                net0: network,
                description: JSON.stringify(description)
            }

            this.createLXC(lxc_data).then(task_id => {
                resolve(task_id);
            }).catch(err => {
                reject(err);
                return;
            })
        });
    }

    createLXC(lxcData) {
        return new Promise((resolve, reject) => {

            this.getRandomOnlineNode().then(PVENode => {
                if (PVENode == null) {
                    reject("No PVE node avaliable!")
                    return;
                }

                const path = "/nodes/" + PVENode.node + "/lxc"
                return ProxmoxController.makeRequest(ProxmoxController.RequestMethods.POST, path, lxcData)
            }).then(task_id => {
                resolve(task_id)
            }).catch(err => {
                reject(err)
                return;
            })
        })
    }
}

const promoxHelper = new PromoxHelper();

module.exports = promoxHelper;