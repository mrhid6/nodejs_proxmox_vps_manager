const redisAdapter = require('socket.io-redis');

const config = require("./server_config");
const logger = require("./server_logger");

const Mrhid6Utils = require("../Mrhid6Utils");
const NET = new Mrhid6Utils.NetworkServer();

//NET._debugTriggeredEvents = true;

NET.init = function (io) {
    NET.setupEventHandlers();
    io.set('transports', ['websocket']);
    io.adapter(redisAdapter({
        host: 'localhost',
        port: 6379
    }));

    NET.startClientListener(io);
};

NET.setupEventHandlers = function () {
    NET.addEventHandler("connection", function (Packet) {
        const socket = Packet.getPacketSocket();
        var clientIp = socket.request.connection.remoteAddress;
        logger.log('New connection from: ' + clientIp);
        logger.log("Sockets: " + NET.getAllServerSockets().length);
    });

    NET.addEventHandler("disconnect", function () {
        logger.log("Client Disconnected!");
        logger.log("Sockets: " + NET.getAllServerSockets().length);
    });
};

module.exports = NET;