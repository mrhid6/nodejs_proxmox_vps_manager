const getIPRange = require('get-ip-range');

var config = require("./server_config");
var DB = require("./server_db");
var logger = require("./server_logger");
var Cleanup = require("./server_cleanup");
const NET = require("./server_net");
const RedisClient = require("./server_redis");
const Mrhid6Utils = require("../Mrhid6Utils");


const ProxmoxHelper = require("./server_proxmox_helper");

// Objects
const iPVENode = require("../objects/obj_pve_node");
const iPVEStorage = require("../objects/obj_pve_storage");
const iResourcePlan = require("../objects/obj_resource_plan");
const iIpAddress = require("../objects/obj_ip_address");
const iVM = require("../objects/obj_vm");
const iVMTemplate = require("../objects/obj_vm_template");
const iClient = require("../objects/obj_client");
const iVMPackage = require("../objects/obj_vm_package");

class VPSManager {
    constructor() {
        this.loaded = {
            pve_nodes_from_cluster: false,
            pve_storage_from_cluster: false
        }
    }

    init() {
        this.setupEventHandlers();
        Mrhid6Utils.Tools.declareSerializedClasses(
            iPVENode,
            iPVEStorage,
            iResourcePlan,
            iIpAddress,
            iVM,
            iVMTemplate,
            iClient
        );
    }

    setupEventHandlers() {
        NET.addEventHandler("packet.client.get.pvenodes", Packet => {
            this.sendPVENodesToClient(Packet);
        })

        NET.addEventHandler("packet.client.get.resourceplans", Packet => {
            this.sendResourcePlansToClient(Packet);
        })

        NET.addEventHandler("packet.client.get.ipaddresses", Packet => {
            this.sendIpAddressesToClient(Packet);
        })

        NET.addEventHandler("packet.client.get.vms", Packet => {
            this.sendVirtualMachinesToClient(Packet);
        })

        NET.addEventHandler("packet.client.get.pve.vms", Packet => {
            this.getPVEVirtualMachinesForClient(Packet);
        })

        NET.addEventHandler("packet.client.get.vmtemplates", Packet => {
            this.sendVMTemplatesToClient(Packet);
        })

        NET.addEventHandler("packet.client.get.clients", Packet => {
            this.sendClientsToClient(Packet);
        })

        NET.addEventHandler("packet.client.rescan.pvenodes", Packet => {
            this.rescanPVENodes(Packet);
        })

        NET.addEventHandler("packet.client.rescan.pvestorage", Packet => {
            this.rescanPVEStorage(Packet);
        })

        NET.addEventHandler("packet.client.rescan.pvetemplates", Packet => {
            this.rescanPVEVMTemplates(Packet);
        })

        NET.addEventHandler("packet.client.add.resourceplan", Packet => {
            this.addNewResourcePlan(Packet);
        })

        NET.addEventHandler("packet.client.add.iprange", Packet => {
            this.addNewIpRange(Packet);
        })

        NET.addEventHandler("packet.client.new.vm", Packet => {
            this.createVirtualMachine(Packet);
        })
    }

    load() {
        this.waitForProxmoxHelper().then(() => {
            this.loadData()
        })
    }

    waitForProxmoxHelper() {
        return new Promise((resolve, reject) => {
            const interval = setInterval(() => {
                if (ProxmoxHelper.controllerLoaded == true) {
                    clearInterval(interval)
                    resolve();
                }
            }, 100)
        })
    }

    loadData() {
        this.checkDataInRedis("ProxVPSMgr.PVENodes").then(result => {
            if (result == null) {
                return this.loadPVENodeDataFromDB()
            } else {
                return this.loadDataFromRedis("ProxVPSMgr.PVENodes");
            }
        })

        this.checkDataInRedis("ProxVPSMgr.ResPlans").then(result => {
            if (result == null) {
                return this.loadResourcePlansFromDB()
            } else {
                return this.loadDataFromRedis("ProxVPSMgr.ResPlans");
            }
        })

        this.checkDataInRedis("ProxVPSMgr.IPAddresses").then(result => {
            if (result == null) {
                return this.loadIPAddressesFromDB()
            } else {
                return this.loadDataFromRedis("ProxVPSMgr.IPAddresses");
            }
        })

        this.checkDataInRedis("ProxVPSMgr.VMs").then(result => {
            if (result == null) {
                return this.loadVMsFromDB()
            } else {
                return this.loadDataFromRedis("ProxVPSMgr.VMs");
            }
        })

        this.checkDataInRedis("ProxVPSMgr.VMTemplates").then(result => {
            if (result == null) {
                return this.loadVMTemplatesFromDB()
            } else {
                return this.loadDataFromRedis("ProxVPSMgr.VMTemplates");
            }
        })

        this.checkDataInRedis("ProxVPSMgr.Clients").then(result => {
            if (result == null) {
                return this.loadClientsFromDB()
            } else {
                return this.loadDataFromRedis("ProxVPSMgr.Clients");
            }
        })
    }

    checkDataInRedis(path) {
        return RedisClient.getAsync(path)
    }

    saveDataToRedis(path, data) {
        return new Promise((resolve, reject) => {
            const serialized_data = Mrhid6Utils.Tools.serialize(data);
            RedisClient.watch(path)

            var multi = RedisClient.multi();
            multi.set(path, serialized_data).execAsync().then(res => {
                resolve();
            }).catch(err => {
                console.log("multi")
                console.log(err);
            })
        })
    }

    loadPVENodeDataFromDB() {
        return new Promise((resolve, reject) => {
            DB.queryUsingSQLFile(__basedir + "/sql/pve/load_pve_nodes.sql").then(nodes => {
                const NODES = [];
                const storagePromises = [];

                for (let i = 0; i < nodes.length; i++) {
                    const node = nodes[i];
                    const PVENode = new iPVENode(
                        node.pve_node_id,
                        node.pve_node_name,
                        node.pve_node_status
                    );

                    NODES.push(PVENode)

                    storagePromises.push(this.loadPVEStorageForPVENode(PVENode));
                }

                Promise.all(storagePromises).then((values) => {
                    for (let i = 0; i < values.length; i++) {
                        const storage = values[i];
                        const A = NODES[i];


                        for (let j = 0; j < storage.length; j++) {
                            A.addStorage(storage[j]);
                        }
                    }
                    this.saveDataToRedis("ProxVPSMgr.PVENodes", NODES).then(() => {
                        resolve(NODES);
                    })
                })

            })
        })

    }

    loadPVEStorageForPVENode(PVENode) {
        return new Promise((resolve, reject) => {
            const mysqlData = [
                PVENode.getId()
            ]

            DB.queryUsingSQLFile(__basedir + "/sql/pve/load_pve_storage.sql", mysqlData).then(rows => {
                const STORAGE = [];

                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];
                    const PVEStorage = new iPVEStorage(
                        row.pve_storage_id,
                        row.pve_storage_name,
                        (row.pve_storage_shared == 1),
                        row.pve_storage_allowed_content.split(",")
                    )
                    STORAGE.push(PVEStorage)
                }
                resolve(STORAGE)
            })
        });
    }

    loadDataFromRedis(path) {
        return new Promise((resolve, reject) => {
            RedisClient.getAsync(path).then(result => {
                if (result != null) {
                    resolve(Mrhid6Utils.Tools.deserialize(result))
                } else {
                    reject("Redis resulted in null data!")
                }
            })
        })
    }

    getPVENodeByName = async name => {
        const nodes = await this.loadDataFromRedis("ProxVPSMgr.PVENodes");
        return nodes.find(el => el.getName() == name);
    }

    getAllPVENodes = async () => {
        const nodes = await this.loadDataFromRedis("ProxVPSMgr.PVENodes");
        return nodes;
    }

    getPVEStorageById = async (id) => {
        const nodes = await this.getAllPVENodes()
        const PVENode = nodes.find(el => el.hasStorageWithId(id))
        if (PVENode == null) return null;
        return PVENode.getStorageById(id);
    }

    getResourcePlanById = async id => {
        const resource_plans = await this.loadDataFromRedis("ProxVPSMgr.ResPlans");
        return resource_plans.find(el => el.getId() == id);
    }

    getAllResourcePlans = async () => {
        const resource_plans = await this.loadDataFromRedis("ProxVPSMgr.ResPlans");
        return resource_plans;
    }

    getIPAddressById = async id => {
        const ip_addresses = await this.loadDataFromRedis("ProxVPSMgr.IPAddresses");
        return ip_addresses.find(el => el.getId() == id);
    }

    getIPAddressByValue = async value => {
        const ip_addresses = await this.loadDataFromRedis("ProxVPSMgr.IPAddresses");
        return ip_addresses.find(el => el.getValue() == value);
    }

    getAllIpAddresses = async () => {
        const ip_addresses = await this.loadDataFromRedis("ProxVPSMgr.IPAddresses");
        return ip_addresses;
    }

    getVMById = async id => {
        const vms = await this.loadDataFromRedis("ProxVPSMgr.VMs");
        return vms.find(el => el.getVMId() == id);
    }

    getNextVMId = async () => {
        const vms = await this.loadDataFromRedis("ProxVPSMgr.VMs");
        let last_id = 100;
        for (let i = 0; i < vms.length; i++) {
            const vm = vms[i];
            if (vm.getVMId() > last_id) last_id = vm.getVMId();
        }

        return (last_id + 1);
    }

    getAllVirtualMachines = async () => {
        const vms = await this.loadDataFromRedis("ProxVPSMgr.VMs");
        return vms;
    }

    getVMTemaplateById = async id => {
        const vm_templates = await this.loadDataFromRedis("ProxVPSMgr.VMTemplates");
        return vm_templates.find(el => el.getId() == id);
    }

    getAllVMTemplates = async () => {
        const templates = await this.loadDataFromRedis("ProxVPSMgr.VMTemplates");
        return templates;
    }

    getClientById = async id => {
        const clients = await this.loadDataFromRedis("ProxVPSMgr.Clients");
        return clients.find(el => el.getId() == id);
    }
    getAllClients = async () => {
        const clients = await this.loadDataFromRedis("ProxVPSMgr.Clients");
        return clients;
    }

    loadResourcePlansFromDB() {
        return new Promise((resolve, reject) => {
            DB.queryUsingSQLFile(__basedir + "/sql/settings/load_resource_plans.sql").then(rows => {
                const RESOURCEPLANS = [];
                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];
                    const ResourcePlan = new iResourcePlan(
                        row.resource_plan_id,
                        row.resource_plan_name,
                        row.resource_plan_type,
                        row.resource_plan_cpulimit,
                        row.resource_plan_ramlimit,
                        row.resource_plan_diskspace
                    )
                    RESOURCEPLANS.push(ResourcePlan)
                }

                this.saveDataToRedis("ProxVPSMgr.ResPlans", RESOURCEPLANS).then(() => {
                    resolve(RESOURCEPLANS);
                })
            })
        })
    }

    loadIPAddressesFromDB() {
        return new Promise((resolve, reject) => {
            DB.queryUsingSQLFile(__basedir + "/sql/settings/load_ip_addresses.sql").then(rows => {
                const IPAddresses = [];
                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];
                    const IPAddress = new iIpAddress(
                        row.ip_id,
                        row.ip_value,
                        row.ip_cidr,
                        row.ip_default_gw,
                        row.ip_assigned_vm_id
                    )
                    IPAddresses.push(IPAddress)
                }

                this.saveDataToRedis("ProxVPSMgr.IPAddresses", IPAddresses).then(() => {
                    resolve(IPAddresses);
                })
            })
        })
    }

    loadVMsFromDB() {
        return new Promise((resolve, reject) => {
            DB.queryUsingSQLFile(__basedir + "/sql/vms/load_vms.sql").then(rows => {
                const VMs = [];
                const resource_promises = [];
                const ip_promises = [];

                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];

                    const VM = new iVM(
                        row.vm_id,
                        row.vm_hostname,
                        row.vm_type,
                        null,
                        null,
                        row.vm_client_id
                    )
                    VMs.push(VM)

                    resource_promises.push(this.getResourcePlanById(row.vm_resource_plan_id))
                    ip_promises.push(this.getIPAddressById(row.vm_ipaddress_id));

                }

                Promise.all(resource_promises).then(values => {
                    for (let i = 0; i < values.length; i++) {
                        const resource_plan = values[i];
                        const A = VMs[i];
                        A.setResourcePlan(resource_plan);
                    }
                    return Promise.all(ip_promises)
                }).then(values => {
                    for (let i = 0; i < values.length; i++) {
                        const ip_address = values[i];
                        const A = VMs[i];
                        A.setIpAddress(ip_address);
                    }
                    this.saveDataToRedis("ProxVPSMgr.VMs", VMs).then(() => {
                        resolve(VMs);
                    })
                });
            })
        })
    }

    loadClientsFromDB() {
        return new Promise((resolve, reject) => {
            DB.queryUsingSQLFile(__basedir + "/sql/clients/load_clients.sql").then(rows => {
                const CLIENTS = [];
                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];

                    const Client = new iClient(
                        row.client_id,
                        row.client_email,
                        row.client_password,
                        row.client_firstname,
                        row.client_lastname,
                    )
                    CLIENTS.push(Client)

                }

                this.saveDataToRedis("ProxVPSMgr.Clients", CLIENTS).then(() => {
                    resolve(CLIENTS);
                })
            })
        });
    }

    loadVMTemplatesFromDB() {
        return new Promise((resolve, reject) => {
            DB.queryUsingSQLFile(__basedir + "/sql/vms/load_vm_templates.sql").then(rows => {
                const TEMPLATES = [];
                for (let i = 0; i < rows.length; i++) {
                    const row = rows[i];

                    const VMTemplate = new iVMTemplate(
                        row.vm_template_id,
                        row.vm_template_name,
                        row.vm_template_location
                    )
                    TEMPLATES.push(VMTemplate)

                }

                this.saveDataToRedis("ProxVPSMgr.VMTemplates", TEMPLATES).then(() => {
                    resolve(TEMPLATES);
                })
            })
        });
    }

    sendPVENodesToClient(Packet) {
        this.getAllPVENodes().then(nodes => {
            if (nodes == null) return

            const serialized_data = Mrhid6Utils.Tools.serialize(nodes)
            const ResPacket = NET.createResponsePacket(Packet, serialized_data)
            NET.sendServerPacket(ResPacket);
        })
    }

    sendResourcePlansToClient(Packet) {
        this.getAllResourcePlans().then(resource_plans => {
            if (resource_plans == null) return

            const serialized_data = Mrhid6Utils.Tools.serialize(resource_plans)
            const ResPacket = NET.createResponsePacket(Packet, serialized_data)
            NET.sendServerPacket(ResPacket);
        })
    }

    sendIpAddressesToClient(Packet) {
        this.getAllIpAddresses().then(ip_addresses => {
            if (ip_addresses == null) return

            const serialized_data = Mrhid6Utils.Tools.serialize(ip_addresses)
            const ResPacket = NET.createResponsePacket(Packet, serialized_data)
            NET.sendServerPacket(ResPacket);
        })
    }

    sendVirtualMachinesToClient(Packet) {
        this.getAllVirtualMachines().then(vms => {
            if (vms == null) return

            const serialized_data = Mrhid6Utils.Tools.serialize(vms)
            const ResPacket = NET.createResponsePacket(Packet, serialized_data)
            NET.sendServerPacket(ResPacket);
        })
    }

    sendVMTemplatesToClient(Packet) {
        this.getAllVMTemplates().then(templates => {
            if (templates == null) return

            const serialized_data = Mrhid6Utils.Tools.serialize(templates)
            const ResPacket = NET.createResponsePacket(Packet, serialized_data)
            NET.sendServerPacket(ResPacket);
        })
    }


    sendClientsToClient(Packet) {
        this.getAllClients().then(clients => {
            if (clients == null) return

            const CLIENTS = []

            for (let i = 0; i < clients.length; i++) {
                const client = clients[i];
                const client_clone = Object.assign(Object.create(Object.getPrototypeOf(client)), client)

                client_clone._password = null
                CLIENTS.push(client_clone);
            }

            const serialized_data = Mrhid6Utils.Tools.serialize(CLIENTS)
            const ResPacket = NET.createResponsePacket(Packet, serialized_data)
            NET.sendServerPacket(ResPacket);
        })
    }

    rescanPVENodes(Packet) {

        ProxmoxHelper.getNodes().then(nodes => {
            const promises = [];
            for (let i = 0; i < nodes.length; i++) {
                const node = nodes[i];
                const mysqlData = [
                    node.node,
                    node.status
                ]
                promises.push(DB.queryUsingSQLFile(__basedir + "/sql/pve/insert_or_update_node.sql", mysqlData))
            }

            Promise.all(promises).then(() => {
                return this.loadPVENodeDataFromDB();
            }).then(() => {
                const P = NET.createPacket("packet.client.get.pvenodes", null, Packet.getPacketSocket());
                this.sendPVENodesToClient(P)
            }).catch(err=>{
                
            })
        })
    }

    rescanPVEStorage(Packet) {
        DB.queryUsingSQLFile(__basedir + "/sql/pve/truncate_pve_storage.sql").then(() => {
            return ProxmoxHelper.getClusterStorage()
        }).then(storages => {
            const promises = [];
            for (let i = 0; i < storages.length; i++) {
                const storage = storages[i];
                const nodeName = storage.node;
                const storageName = storage.storage;
                const shared = storage.shared;

                promises.push(this.storePVEStorageInfo(nodeName, storageName, shared))
            }

            Promise.all(promises).then((values) => {
                return this.loadPVENodeDataFromDB()
            }).then(() => {
                const P = NET.createPacket("packet.client.get.pvenodes", null, Packet.getPacketSocket());
                this.sendPVENodesToClient(P)
            })
        })
    }

    rescanPVEVMTemplates(Packet) {
        DB.queryUsingSQLFile(__basedir + "/sql/vms/truncate_vm_templates.sql").then(() => {
            return ProxmoxHelper.getRandomOnlineNode()
        }).then(PVENode => {

            const nodeName = PVENode.node;
            this.getPVENodeByName(nodeName).then(Node => {
                const storages = Node.getAllTemplateStorage().filter(el => el.isShared() == true);

                let ALLPVETemplates = [];
                const template_promises = [];
                for (let i = 0; i < storages.length; i++) {
                    const storage = storages[i];
                    template_promises.push(ProxmoxHelper.getVirtualMachineTemplates(nodeName, storage.getName()))
                }

                Promise.all(template_promises).then(values => {
                    for (let i = 0; i < values.length; i++) {
                        const value = values[i];

                        for (let j = 0; j < value.length; j++) {
                            const template = value[j];

                            const location = template.volid;
                            const roughName = (location.split("/")[1]).replace(".tar.gz", "")

                            const VMTemplate = new iVMTemplate(
                                0,
                                roughName,
                                location
                            )

                            ALLPVETemplates.push(VMTemplate)
                        }
                    }

                    return this.storeVMTemplateInDB(ALLPVETemplates)
                }).then(() => {
                    return this.loadVMTemplatesFromDB();
                }).then(() => {
                    const P1 = NET.createPacket("packet.client.get.vmtemplates", null, Packet.getPacketSocket());
                    this.sendVMTemplatesToClient(P1);
                })

            })

        })
    }

    storeVMTemplateInDB(VMTemplates) {
        return new Promise((resolve, reject) => {
            const promises = [];
            for (let i = 0; i < VMTemplates.length; i++) {
                const VMTemplate = VMTemplates[i];
                const mysqlData = [
                    VMTemplate.getName(),
                    VMTemplate.getLocation()
                ]

                promises.push(DB.queryUsingSQLFile(__basedir + "/sql/vms/create_vm_template.sql", mysqlData))
            }

            Promise.all(promises).then(() => {
                resolve();
            })
        });
    }

    getPVEVirtualMachinesForClient(Packet) {
        const VMs = [];

        ProxmoxHelper.getVirtualMachines().then(PVEVms => {

            const vm_promises = [];
            for (let i = 0; i < PVEVms.length; i++) {
                const PVEVM = PVEVms[i];

                vm_promises.push(ProxmoxHelper.getVirtualMachineInNode(PVEVM.node, PVEVM.id));
            }
            const resource_promises = [];
            const ip_promises = [];

            Promise.all(vm_promises).then(values => {
                for (let i = 0; i < values.length; i++) {
                    const vmData = values[i];

                    if (vmData.description == "" || vmData.description == null) {
                        continue;
                    }

                    const vmDetail = JSON.parse(vmData.description);

                    const VM = new iVM(
                        vmDetail.vmid,
                        vmData.hostname,
                        null,
                        null,
                        vmDetail.clientid
                    )

                    VMs.push(VM)

                    resource_promises.push(this.getResourcePlanById(vmDetail.resource_plan))
                    ip_promises.push(this.getIPAddressById(vmDetail.ipaddress));
                }
                return Promise.all(resource_promises)
            }).then(values => {
                for (let i = 0; i < values.length; i++) {
                    const resource_plan = values[i];
                    const A = VMs[i];
                    A.setResourcePlan(resource_plan);
                }
                return Promise.all(ip_promises)
            }).then(values => {
                for (let i = 0; i < values.length; i++) {
                    const ip_address = values[i];
                    const A = VMs[i];
                    A.setIpAddress(ip_address);
                }

                const serialized_data = Mrhid6Utils.Tools.serialize(VMs)
                const ResPacket = NET.createResponsePacket(Packet, serialized_data);
                NET.sendServerPacket(ResPacket);
            });
        });
    }

    storePVEStorageInfo(nodeName, storageName, shared) {
        return new Promise((resolve, reject) => {
            this.getPVENodeByName(nodeName).then(PVENode => {
                if (PVENode == null) {
                    console.log("Node is Null!")
                    return;
                }

                ProxmoxHelper.getStorageFromNode(PVENode.getName(), storageName).then(storageInfo => {
                    const mysqlData = [
                        PVENode.getId(),
                        storageName,
                        shared,
                        storageInfo.content
                    ]

                    return DB.queryUsingSQLFile(__basedir + "/sql/pve/insert_pve_storage.sql", mysqlData)
                }).then(() => {
                    resolve()
                })
            })
        })
    }

    addNewResourcePlan(Packet) {
        return new Promise((resolve, reject) => {
            const pData = Packet.getPacketData();
            const mysqlData = [
                pData.name,
                pData.type,
                pData.cpu,
                pData.ram,
                pData.disk
            ]

            DB.queryUsingSQLFile(__basedir + "/sql/settings/create_resource_plan.sql", mysqlData).then(() => {
                return this.loadResourcePlansFromDB();
            }).then(() => {
                const P1 = NET.createPacket("packet.client.get.resourceplans", null, Packet.getPacketSocket())
                this.sendResourcePlansToClient(P1);

                const P2 = NET.createResponsePacket(Packet, {
                    result: "success"
                })
                NET.sendServerPacket(P2);
            })
        });
    }

    addNewIpRange(Packet) {
        const pData = Packet.getPacketData();

        const ipAddresses = getIPRange(pData.start, pData.end).value;

        if (ipAddresses.length == 0) {
            const P2 = NET.createResponsePacket(Packet, {
                result: "error",
                error: "No IP addresses to add. check start and end ip addresses!"
            })
            NET.sendServerPacket(P2);
            return;
        }

        const promises = [];
        for (let i = 0; i < ipAddresses.length; i++) {
            const ipaddress = ipAddresses[i];
            if (ipaddress.endsWith(".0") || ipaddress.endsWith(".255"))
                continue;

            promises.push(this.storeNewIpAddress(ipaddress, pData));
        }

        Promise.all(promises).then(() => {
            return this.loadIPAddressesFromDB();
        }).then(() => {
            const P1 = NET.createPacket("packet.client.get.ipaddresses", null, Packet.getPacketSocket())
            this.sendIpAddressesToClient(P1);

            const P2 = NET.createResponsePacket(Packet, {
                result: "success"
            })
            NET.sendServerPacket(P2);
        })
    }

    storeNewIpAddress(ipaddress, pData) {
        return new Promise((resolve, reject) => {
            this.getIPAddressByValue(ipaddress).then(result => {

                if (result != null) {
                    resolve();
                    return;
                }

                const mysqlData = [
                    ipaddress,
                    pData.cidr,
                    pData.defgw
                ]
                DB.queryUsingSQLFile(__basedir + "/sql/settings/create_ip_address.sql", mysqlData).then(() => {
                    resolve();
                })
            })

        });
    }


    // Create Virtual Machine

    createVirtualMachine(Packet) {
        return new Promise((resolve, reject) => {
            let vm_package = null;

            this.buildNewVMPackage(Packet.getPacketData()).then(VMPackage => {
                vm_package = VMPackage;
                return ProxmoxHelper.createVMFromVMPackage(vm_package);
            }).then(() => {
                const ResPacket = NET.createResponsePacket(Packet, {
                    result: "success"
                });

                NET.sendServerPacket(ResPacket);
                return this.storeNewVMInDB(vm_package, Packet)
            }).then(() => {
                const ResPacket = NET.createResponsePacket(Packet, {
                    result: "success"
                });

                NET.sendServerPacket(ResPacket);
            }).catch(err => {
                let errorMsg = "";
                if (typeof err == String) {
                    errorMsg = err;
                } else {
                    errorMsg = err.response.statusText;
                }

                const ResPacket = NET.createResponsePacket(Packet, {
                    result: "error",
                    error_msg: errorMsg
                });

                NET.sendServerPacket(ResPacket);
            })
        })
    }

    buildNewVMPackage(data) {
        return new Promise((resolve, reject) => {
            const data_promises = [];

            data_promises.push(this.getNextVMId())
            data_promises.push(this.getVMTemaplateById(data.vm_template))
            data_promises.push(this.getIPAddressById(data.vm_ip))
            data_promises.push(this.getPVEStorageById(data.vm_storage))
            data_promises.push(this.getResourcePlanById(data.vm_resplan))
            data_promises.push(this.getClientById(data.vm_client))

            Promise.all(data_promises).then(values => {
                let VM_Id = null
                let VMTemplate = null;
                let IPAddress = null;
                let ResourcePlan = null;
                let Client = null;
                let PVEStorage = null;

                for (let i = 0; i < values.length; i++) {
                    const value = values[i];
                    if (isNaN(value) == false) {
                        VM_Id = value;
                    } else if (value instanceof iVMTemplate) {
                        VMTemplate = value;
                    } else if (value instanceof iIpAddress) {
                        IPAddress = value;
                    } else if (value instanceof iPVEStorage) {
                        PVEStorage = value;
                    } else if (value instanceof iResourcePlan) {
                        ResourcePlan = value;
                    } else if (value instanceof iClient) {
                        Client = value;
                    }
                }

                const VMPackage = new iVMPackage(
                    VM_Id,
                    data.vm_type,
                    data.vm_name,
                    VMTemplate,
                    IPAddress,
                    PVEStorage,
                    ResourcePlan,
                    Client
                )

                resolve(VMPackage)

            })

        })
    }

    storeNewVMInDB(VMPackage, Packet) {
        return new Promise((resolve, reject) => {
            const mysqlData = [
                VMPackage.getVMId(),
                VMPackage.getResourcePlan().getId(),
                VMPackage.getIPAddress().getId(),
                VMPackage.getType(),
                VMPackage.getClient().getId(),
                VMPackage.getName()
            ]
            DB.queryUsingSQLFile(__basedir + "/sql/vms/create_vm.sql", mysqlData).then(() => {
                return this.loadVMsFromDB()
            }).then(() => {
                return this.assignIpAddressToVM(VMPackage.getIPAddress(), VMPackage.getVMId());
            }).then(() => {
                const P1 = NET.createPacket("packet.client.get.vms", null, Packet.getPacketSocket());
                this.sendVirtualMachinesToClient(P1);

                const P2 = NET.createPacket("packet.client.get.ipaddresses", null, Packet.getPacketSocket());
                this.sendIpAddressesToClient(P2);
                resolve()
            })
        })
    }

    assignIpAddressToVM(IPAddress, vmid) {
        return new Promise((resolve, reject) => {
            const mysqlData = [
                vmid,
                IPAddress.getId()
            ]
            DB.queryUsingSQLFile(__basedir + "/sql/settings/assign_ip_vm.sql", mysqlData).then(() => {
                return this.loadIPAddressesFromDB();
            }).then(() => {
                resolve();
            })

        })
    }

}

const vpsManager = new VPSManager();

module.exports = vpsManager;