const config = require("./server_config");
const Mrhid6Utils = require("../Mrhid6Utils");

const DB = new Mrhid6Utils.DatabaseHelperNew(config);

DB.createConnection();

module.exports = DB;