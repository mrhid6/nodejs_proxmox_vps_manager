const axios = require("axios")
var https = require('https')
const qs = require('querystring');
const ping = require("ping");

const config = require("./server_config");
const logger = require("./server_logger");

const axiosInstance = axios.create({
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
    },
    httpsAgent: new https.Agent({
        rejectUnauthorized: false
    })
});


class ProxmoxController {
    constructor() {
        this.apiUrl = "";
        this.authToken = {
            CSRF: '',
            PVEAuth: '',
            timeStamp: 0
        }

        this.RequestMethods = {
            GET: 'GET',
            POST: 'POST',
            DEL: 'DEL'
        }

        this.onlineNodes = [];
    }

    init() {
        return new Promise((resolve, reject) => {
            const nodes = config.get("proxmox.nodes");

            if (nodes.length == 0) {
                const err = new Error("Config Setting: proxmox.nodes must contain at least one node!")
                reject(err);
                return;
            }

            this.testOnlineNodes_Ping().then(() => {
                return this.authorize()
            }).then(() => {
                resolve();
            }).catch(err => {
                reject(err);
                return;
            })
        })
    }

    testOnlineNodes_Ping() {
        return new Promise((resolve, reject) => {
            const nodes = config.get("proxmox.nodes");
            const ping_promises = [];
            this.onlineNodes = [];

            for (let i = 0; i < nodes.length; i++) {
                const node = nodes[i];
                ping_promises.push(ping.promise.probe(node, {
                    timeout: 10,
                    min_reply: 2
                }))
            }
            Promise.all(ping_promises).then(values => {
                for (let i = 0; i < values.length; i++) {
                    const value = values[i];
                    if (value.alive == true) {
                        this.onlineNodes.push(value.host)
                    }
                }

                if (this.onlineNodes.length == 0) {
                    reject("No availible proxmox nodes are online!");
                    return;
                }

                resolve();

            })

        });
    }

    authorize() {
        return new Promise((resolve, reject) => {

            if ((this.authToken.timeStamp + (1 * 60 * 1000)) > new Date().getTime()) {
                resolve();
                return;
            }

            this.testOnlineNodes_Ping().then(() => {
                console.log(this.onlineNodes);
                const node = this.onlineNodes[Math.floor(Math.random() * this.onlineNodes.length)];
                console.log(node);

                this.apiUrl = "https://" + node + ":8006/api2/json"
                console.log(this.apiUrl);

                logger.log("[Proxmox-Controller] [AUTH] - Authentication Started ..");
                const postBody = qs.stringify({
                    username: config.get("proxmox.username"),
                    password: config.get("proxmox.password")
                })
                return axiosInstance.post(this.apiUrl + "/access/ticket", postBody)
            }).then(response => {
                this.authToken.CSRF = response.data.data.CSRFPreventionToken
                this.authToken.PVEAuth = response.data.data.ticket
                this.authToken.timeStamp = new Date().getTime();
                logger.log("[Proxmox-Controller] [AUTH] - Authenticated Successfully");
                resolve();
            }).catch(error => {
                console.log(error)
                if (typeof error === "string") {
                    reject(error);
                } else {
                    reject("Promox couldn't authenticate to PVE Host!");
                }
            });


        });
    }

    makeRequest(method, path, data) {
        return new Promise((resolve, reject) => {

            this.authorize().then(() => {
                const requestConfig = {
                    headers: {
                        Cookie: "PVEAuthCookie=" + this.authToken.PVEAuth,
                        CSRFPreventionToken: this.authToken.CSRF
                    }
                }
                if (method == this.RequestMethods.GET) {
                    return axiosInstance.get(this.apiUrl + path, requestConfig)
                } else if (method == this.RequestMethods.POST) {
                    const postBody = qs.stringify(data)
                    return axiosInstance.post(this.apiUrl + path, postBody, requestConfig)
                } else if (method == this.RequestMethods.DEL) {
                    const delBody = qs.stringify(data)
                    return axiosInstance.post(this.apiUrl + path, delBody, requestConfig)
                }
            }).then(response => {
                resolve(response.data.data)
            }).catch(err => {
                reject(err)
            })
        })
    }
}

const proxmoxController = new ProxmoxController();

module.exports = proxmoxController;