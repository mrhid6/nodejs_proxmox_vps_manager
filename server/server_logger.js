const Mrhid6Utils = require("../Mrhid6Utils");

const Config = require("./server_config");
const Logger = new Mrhid6Utils.Logger(Config, {
    basedir: __basedir
});

module.exports = Logger;