var config = require("./server_config");
var DB = require("./server_db");
var logger = require("./server_logger");
var Cleanup = require("./server_cleanup");
const NET = require("./server_net");

const ProxmoxHelper = require("./server_proxmox_helper");
const VPSManager = require("./server_vps_manager");

var ServerApp = {};

ServerApp.init = function (io) {
    logger.log("[APP] [INIT] - Initialization started..");
    ServerApp.setupEventHandlers();
    logger.log("[APP] [INIT] - Initialization completed..");
    ServerApp.postInit(io)
};

ServerApp.setupEventHandlers = function () {
    NET.addEventHandler("packet.client.test", function (socket, e, d) {
        console.log(JSON.stringify(socket.handshake.session, null, 2));
    })
    Cleanup.addEventHandler(function (err) {});
};

ServerApp.postInit = function (io) {
    logger.log("[APP] [POST-INIT] - Post-Initialization started..");
    const DB = require("./server_db");
    DB.testConnection().then(() => {
        NET.init(io);
        ProxmoxHelper.init();
        VPSManager.init();
        logger.log("[APP] [POST-INIT] - Post-Initialization completed..");
        ServerApp.Load();
    }).catch(err => {
        console.log(err);
        logger.error("[APP] [POST-INIT] - Database cant be connected!");
        Cleanup.cleanup();
    });
};

ServerApp.Load = function () {
    logger.log("[APP] [LOAD] - Loading started..");
    VPSManager.load();
    logger.log("[APP] [LOAD] - Loading completed..");
};

module.exports = ServerApp;