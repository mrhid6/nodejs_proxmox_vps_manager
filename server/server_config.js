const fs = require("fs-extra")
const objectpath = require("object-path");

class ServerConfig {
    constructor() {
        this.configPath = __basedir + "/config/config.json"

        if (fs.existsSync(this.configPath) == false) {
            throw new Error("Cannot find config.json [" + this.configPath + "]")
        }
        this.init();
    }

    init() {
        this.configData = require(this.configPath);
    }

    get(key, defaultval) {
        let return_val = objectpath.get(this.configData, key);
        if (return_val == null) {
            this.set(key, defaultval);
        }
        return objectpath.get(this.configData, key);
    }

    set(key, val) {
        objectpath.set(this.configData, key, val);
    }
}

const serverConfig = new ServerConfig();

module.exports = serverConfig;