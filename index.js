global.__basedir = __dirname;



const express = require('express');
const exphbs = require('express-handlebars');
const session = require('express-session');
const RedisStore = require("connect-redis")(session);
const sharedsession = require("express-socket.io-session");

var cors = require('cors');

const path = require('path');
const bodyParser = require('body-parser');

const methodOverride = require('method-override');

const app = express();

var http = require('http').Server(app);
var io = require('socket.io')(http);


const Config = require("./server/server_config");
const logger = require("./server/server_logger")
const DB = require("./server/server_db");

const APP_Server = {}


APP_Server.init = function () {
    APP_Server.startExpress();
}

APP_Server.startExpress = function () {
    logger.log("Starting Express..");

    var cookieParser = require('cookie-parser')

    const expsess = session({
        secret: 'PROXVPSMGR',
        store: new RedisStore({}),
        resave: false,
        saveUninitialized: true,
        cookie: {
            maxAge: (15 * 60 * 1000)
        },
        rolling: true
    });

    app.use(expsess);

    io.use(sharedsession(expsess, {
        autoSave: true
    }));


    app.set('views', path.join(__dirname + '/views'));
    app.engine('.hbs', exphbs({
        defaultLayout: 'main.hbs',
        layoutsDir: path.join(__dirname + '/views/layouts')
    }));
    app.set('view engine', '.hbs');

    // body-parser
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: true
    }));

    var corsOptions = {
        origin: '*',
        optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204 
    }
    app.use(cors(corsOptions));

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });

    // methodOverride
    app.use(methodOverride('_method'));

    app.use("/libraries", express.static(__dirname + '/node_modules'));
    app.use("/public", express.static(__dirname + '/public'));

    app.use(cookieParser());

    app.use("/", require("./routes"));

    const http_port = Config.get("http.port", 3000);

    http.listen(http_port, function (req, res) {
        logger.log("[APP] [INIT] - Server listening on port: (" + http_port + ")..");
        APP_Server.startServer();
    });
}

APP_Server.startServer = function () {
    logger.log("[APP] [INIT] - Server SERVER!");
    require("./server/server_app").init(io);
}

APP_Server.init();